﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Media;
using System.Windows.Forms;

namespace NotepanDotNet
{
    public partial class Form1 : Form
    {
        int command = 0;
        List<string> names = new List<string>() { "Latex Document", "Beamer Presentation", "HTML Document" };
        List<string> commands = new List<string>() { " -o *.pdf", " -t beamer -o *.pdf", " -o *.html" };
        bool saved = true;

        public Form1()
        {
            InitializeComponent();

            openFileDialog1.FileName = "";
            saveFileDialog1.AddExtension = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Properties.Settings.Default.Reset();
            try
            {
                Open(Environment.GetCommandLineArgs()[1].ToString());
            }
            catch { }
        }

        #region Functions
        void Open(string path = "")
        {
            string recent = Properties.Settings.Default["Recent"].ToString();
            if (recent.Split(',').Length >= 5)
            {
                recent = recent.Substring(recent.IndexOf(','));
            }
            recent += openFileDialog1.FileName + ",";
            Properties.Settings.Default["Recent"] = recent;
            Properties.Settings.Default.Save();

            if (saved || MessageBox.Show("Unsaved changes will be lost, are you sure you wish to continue?", "notepan.NET", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                richTextBox1.Clear();

                if (path == "" && openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
                   openFileDialog1.FileName.Length > 0)
                {
                    richTextBox1.LoadFile(openFileDialog1.FileName, RichTextBoxStreamType.PlainText);
                }
                else
                {
                    richTextBox1.LoadFile(path, RichTextBoxStreamType.PlainText);
                    openFileDialog1.FileName = path;
                }
            }
        }

        void SaveAs()
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SaveFile(saveFileDialog1.FileName);
            }
        }

        void SaveFile(string path)
        {
            toolStripStatusLabel1.Text = names[command];
            if (path != "")
            {
                richTextBox1.SaveFile(path, RichTextBoxStreamType.PlainText);
                openFileDialog1.FileName = path;
                saved = true;
            }
            else
            {
                SaveAs();
            }
        }

        void Compile(string path)
        {
            toolStripProgressBar1.Visible = true;
            toolStripProgressBar1.Value = 0;
            Process p = new Process();

            try
            {
                toolStripProgressBar1.Value = 50;
                p.StartInfo.FileName = "pandoc.exe";
                p.StartInfo.Arguments = path + commands[command].Replace("*", path.Substring(0, path.LastIndexOf(".")));
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.Start();
                toolStripProgressBar1.Value = 50;
                while (!p.HasExited) ;
                toolStripProgressBar1.Value = 100;
                SystemSounds.Asterisk.Play();
                //Process.Start(path.Substring(0, path.LastIndexOf(".")) + commands[command].Substring(commands[command].LastIndexOf(".")));
                Process.Start(path.Substring(0, path.LastIndexOf("\\")) + "\\");

            }
            catch (Exception e)
            {
                toolStripProgressBar1.Value = 0;
                SystemSounds.Hand.Play();
                MessageBox.Show(e.Message);
            }
            toolStripProgressBar1.Value = 0;
        }

        private void ItemClick(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            Open(item.Name);
        }
        #endregion

        #region Buttons n' Shiet
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            saved = false;
            toolStripProgressBar1.Visible = false;
        }

        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (saved || MessageBox.Show("Unsaved changes will be lost, are you sure you wish to continue?", "notepan.NET", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                richTextBox1.Clear();
            }
        }
        private void fileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            recentItemsToolStripMenuItem1.DropDownItems.Clear();
            foreach (string entry in Properties.Settings.Default["Recent"].ToString().Split(','))
            {
                if (entry != "")
                {
                    ToolStripItem item = new ToolStripMenuItem();
                    item.Text = entry.Substring(entry.LastIndexOf('\\') + 1);
                    item.Name = entry;
                    item.Click += new EventHandler(ItemClick);
                    recentItemsToolStripMenuItem1.DropDownItems.Add(item);
                }
            }
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SaveFile(openFileDialog1.FileName);
        }

        private void saveAsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SaveAs();
        }
        
        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void compileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SaveFile(openFileDialog1.FileName);
            Compile(openFileDialog1.FileName);
        }

        private void cutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        private void copyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        private void pasteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        private void undoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            richTextBox1.Undo();
        }

        private void redoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            richTextBox1.Redo();
        }

        private void selectAllToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectAll();
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string dialog = Microsoft.VisualBasic.Interaction.InputBox(
                "Find what", "Find", 
                richTextBox1.SelectedText, 
                -1, -1
                );
            richTextBox1.Find(dialog);
        }

        private void headerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectedText = "# " + richTextBox1.SelectedText + " #";
        }

        private void boldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectedText = "**" + richTextBox1.SelectedText + "**";
        }

        private void italicsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectedText = "_" + richTextBox1.SelectedText + "_";
        }

        private void strikethroughToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectedText = "~~" + richTextBox1.SelectedText + "~~";
        }

        private void linkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string dialog = Microsoft.VisualBasic.Interaction.InputBox(
                "URL", "Link", 
                Clipboard.GetText(TextDataFormat.Text), 
                -1, -1
                );
            if (dialog.Length > 0)
            {
                richTextBox1.SelectedText = "[" + richTextBox1.SelectedText + "](" + dialog + ")";
            }
        }

        private void imageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string dialog = Microsoft.VisualBasic.Interaction.InputBox(
                "Path or URL", "Image",
                Clipboard.GetText(TextDataFormat.Text),
                -1, -1
                );
            if (dialog.Length > 0)
            {
                richTextBox1.SelectedText = "![" + richTextBox1.SelectedText + "](" + dialog + ")";
            }
        }

        private void latexDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            command = 0;
            toolStripStatusLabel1.Text = names[command];
        }

        private void beamerPresentationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            command = 1;
            toolStripStatusLabel1.Text = names[command];
        }

        private void hTMLDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            command = 2;
            toolStripStatusLabel1.Text = names[command];
        }
        #endregion

    }
}
